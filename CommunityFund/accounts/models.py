from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User

# TODO fix rating system.

#class Rating (models.Models):
 #   user = models.OneToOneField(User)
  #  rating = models.IntegerField(default=0)
   # number_rating = models.IntegerField(default=0)

#model for user interest
class Interests (models.Model):    
    def __str__(self):
	choices_dic= dict(INTERESTS)              # __unicode__ on Python 2
        return choices_dic[self.interests]

#model for user profile 
class UserProfile (models.Model):
    SEX =(
        ('M', 'Male'),
        ('F', 'Female'),
        )
    TYPE = (
        ('F', 'Funder'),
        ('I', 'Initiator'),
        )
    
    user = models.OneToOneField(User)
    birth_date = models.DateField(null=True)
    sex = models.CharField(max_length=1, choices=SEX, null=True)
    account = models.CharField(max_length=1, choices=TYPE, null=False)#Account type either initiator or Funder
    about = models.TextField(max_length=300, null=True)
    interests = models.ManyToManyField(Interests)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(user=instance)
        post_save.connect(create_user_profile, sender=User)
