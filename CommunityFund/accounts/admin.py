from django.contrib import admin

# Register your models here.
from .models import UserProfile, Interests
admin.site.register(UserProfile)
admin.site.register(Interests)