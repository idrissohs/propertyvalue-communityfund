from django.conf.urls import patterns, include, url
from .views import RegisterProfile
from .views import ProfilePage

urlpatterns = patterns('',
    url(r'^registerprofile$', RegisterProfile , name='registerprofile'),
    url(r'^profile/(?P<user_id>\w+)/$', ProfilePage, name='profile'),
    )

