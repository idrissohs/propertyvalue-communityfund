from django.shortcuts import render
from .forms import ProfileForm
from .models import Interests
from .models import UserProfile
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.views.generic.detail import DetailView
# Create your views here.
@login_required
def RegisterProfile(request):

    if request.method == 'POST':

        profile_form = ProfileForm( data=request.POST)
        if profile_form.is_valid():
            #create profile
            profile = profile_form.save(commit=False)
            profile.user=request.user
            profile.save()
            #redirect base on account type
            if profile.account == 'F':
                return redirect('project.views.funder')
            elif profile.account == 'I':
                return redirect('project.views.initiator')
        else:
            print(profile_form.errors)
        
    profile_form=ProfileForm() 
    return render(request,'project/profileregistration.html', {'profile_form': profile_form} )

# view to display a user's profile
def ProfilePage (request, user_id):
    profile=UserProfile.objects.get( user_id=user_id)
    return render(request, 'project/profilepage.html', {'profile': profile})
