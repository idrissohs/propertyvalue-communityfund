from django.forms import ModelForm
from django import forms
from .models import UserProfile, Interests

#form to collect profile information
class ProfileForm (ModelForm): 
    def __init__(self, *args, **kwargs):
	super(ProfileForm, self).__init__(*args, **kwargs)
    INTERESTS = (
        ('ANIMAL', 'Animal Care'),
        ('SPORT', 'Sports'),
        ('ART', 'Arts'),
        ('EDUCAT', 'Education'),
        ('ENVIRON', 'Environment'),
        ('HEALTH', 'Health Care'),
        ('JUSTICE', 'Social Justice'),
        ('TECHNO', 'Technology'),
        )

    interests = forms.MultipleChoiceField(label="Interests", widget=forms.CheckboxSelectMultiple, choices=INTERESTS)

    class Meta:
        model = UserProfile
        fields = ['birth_date', 'sex', 'account', 'about']
