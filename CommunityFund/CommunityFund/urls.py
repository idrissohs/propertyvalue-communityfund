from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
	url(r'^$', 'project.views.index', name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'project/login.html'}),
    url(r'', include('project.urls')),
    url(r'', include('accounts.urls')),
)
